---
layout: page
title:  "25.01.2021: Heutige Umweltausschusssitzung: vom Ende der Augsburger Ambitionslücke"
date:   2020-01-25 23:45:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 35
---

*Pressemitteilung vom Augsburger Klimacamp am 25. Januar 2021*

# Heutige Umweltausschusssitzung: vom Ende der Augsburger Ambitionslücke

Am heutigen Montagnachmittag (Tag 209 des Klimacamps) bestätigte der
Umweltausschuss der Stadt Augsburg die vom Klimacamp im Klimabeirat
durchgesetzte Beschlussempfehlung: Augsburg solle sich ein
CO₂-Restbudget von 9,7 Millionen Tonnen ab 1.1.2021 setzen. Nun muss nur
noch der Stadtrat die Entscheidung bestätigen, damit sich die
Ambitionslücke in der städtischen Klimapolitik schließt. (Die
Stadtratsentscheidung war ursprünglich für Donnerstag vorgesehen, wurde
aber wegen Corona verschoben.)

"Unser Wert von 9,7 Millionen Tonnen entsteht durch eine äußerst
wohlwollende Rechnung [1], die historische Emissionen und Kippelemente
außer Acht lässt und unreflektiert dem weichgewaschenen
IPCC-Sonderbericht folgt", erklärt Klimacamperin Charlotte Lauter (18).
Zum Vergleich: Die schwarz-grüne Stadtregierung peilte in ihrem
Koalitionsvertrag etwa 32 Millionen Tonnen an. Die Grünen verkündeten
kürzlich [2], eine Restmenge von 16 Millionen Tonnen ins Auge zu
fassen. "Von dieser Ankündigung waren wir maßlos enttäuscht", so Lauter.

"Die neue Zielsetzung ist also ein großartiger Schritt und Grund zum
Feiern!", freut sich Lauter. "Nur der AfD-Vertreter stimmte gegen das
CO₂-Restbudget." Die Klimacamper\*innen wissen aber auch, dass die Stadt
Augsburg das beschlossene Budget nicht ohne Weichenstellung seitens des
Lands und des Bunds erreichen kann. "Das ist keine Lizenz zum Ausruhen,
sondern ein dringender Aufruf an die Stadt, sich sowohl medienwirksam
als auch in den entsprechenden politischen Gremien mit Nachdruck für
klimagerechte Weichenstellungen stark zu machen."

"Von der geschlossenen Bitte um Zustimmung der CSU im heutigen
Umweltausschuss waren wir positiv überrascht. In der großen
Stadtratssitzung im Februar entscheidet sich, ob die CSU zu ihren Worten
stehen wird", so Lauters Kollegin Linda Ruchti (20).
"Oberbürgermeisterin Weber erklärte uns in unseren Verhandlungen vor
Weihnachten, dass sie in einer Doku gehört hätte, dass die Bekämpfung
der Klimakrise doch noch nicht so eilig sei." Ruchti erinnert daran,
dass die Klimacamper\*innen nur eine einzige angemessene und konkrete
Klimagerechtigkeitsmaßnahme benötigt hätten, um der Stadt einen
Vertrauensvorschuss zu geben und das Camp zu pausieren. "Doch die
gescheiterten Verhandlungen wurden in der überregionalen
Fridays-for-Future-Vernetzung zum Anlass, dass im Frühling und Sommer
noch in vielen weiteren Städten Klimacamps entstehen werden."

Bei aller Freude gibt Lauter aber auch zu bedenken: "Papier und CSU geduldig. Augsburgs Regierung pflegt
eine lange Tradition der Umsetzungslücke." Sie
spielt damit etwa auf das Projekt "Fahrradstadt 2020" an, das 2012
einstimmig vom Stadtrat beschlossen wurde. "Sieben Radachsen sowie
andere Maßnahmen waren geplant, davon setzte die Stadt aber nicht einmal
10 % um, und gerade an den vielen gefährlichen Tunneln und Kreuzungen
ist praktisch nichts passiert", so Lauter. "Die Stadt beschloss auch,
bis 2010 in städtischen Einrichtungen 75% Recyclingpapier einzusetzen.
2017 erreichte sie gerade einmal 48%." Nach Aussage des städtischen
Büros für Nachhaltigkeit belegt Augsburg damit unter allen bayerischen
Großstädten den letzten Platz [3]. "Auch wird der Beschluss aus dem Jahr
2007, bei städtischen Veranstaltungen ausschließlich auf Bio-Verpflegung
zu setzen, konsequent ignoriert."

"Vor uns liegt also trotzdem noch jede Menge Arbeit. Wir werden den Wind
machen, in den die CSU ihre Fahnen hängen wird!", erklärt Ruchti
entschlossen.

[1] https://augsburg.klimacamp.eu/co2-budget/<br>
[2] https://m.youtube.com/watch?v=nohE7suqSmE<br>
[3] https://www.nachhaltigkeit.augsburg.de/fileadmin/nachhaltigkeit/data/Indikatorenbl%C3%A4tter_Zukunftsleitlinien/Indikatorblatt_%C3%962.2_Papiereinkauf_und_Recyclingpapier.pdf
