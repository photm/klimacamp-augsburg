---
layout: page
title: Forderungen
permalink: /forderungen/
nav_order: 3
date: 2020-08-16T13:18:18.107Z
---

# Forderungen

## 1. Einhaltung des Augsburger CO₂-Budgets für die 1,5-Grad-Grenze

### ✅ Öffentliche Kommunikation über die geplanten CO₂-Emissionen

Am 25.1.2021 bestätigte der Umweltausschuss des Augsburger Stadtrats die
Zielsetzung, die wir für eine lokale Einhaltung des Pariser Klimaabkommens
forderten: Dass Augsburg ab dem 1.1.2021 nur noch 9,7 Millionen Tonnen CO₂
ausstößt.

Der schwarz-grüne Koalitionsvertrag visierte dagegen eine dreifache
Überschreitung des Budgets an (34 Millionen Tonnen CO₂ ab 1.1.2020). Die
Forderung nach der öffentlichen Kommunikation entstammte aus dieser Zeit.
Einhergehend mit einer solchen Veröffentlichung hätte eine Rechtfertigung für
die Überschreitung folgen sollen, wie in unserem [offenen Brief an den
Stadtrat](/pages/offeneBriefe/2020-07-23-OB-Forderungen.html) dargelegt. Eine
solche Rechtfertigung ist mit dem neuen Ziel nicht mehr nötig.

Im Übrigen kann unsere Rechnung der 9,7 Millionen Tonnen durchaus [hart
kritisiert werden](/CO2-Budget/).


### ❌ Sozial gerechte Umsetzung von Maßnahmen für Augsburg

Um das Restbudget von 9,7 Millionen Tonnen nicht zu überschreiten, sind
gewaltige strukturelle Veränderungen in Augsburg nötig. Einige Vorschläge sind
Teil der Forderungen 3 und 4. Wir erkennen nicht, dass diese Veränderungen mit
der gebotenen Ernsthaftigkeit und Dringlichkeit angegangen werden.


### ❌ Klimaangepasste resiliente Ökosysteme erhalten

Wertvolle Lebensräume müssen erhalten bleiben. Wir haben dabei nicht nur
Lebensräume direkt auf dem Stadtgebiet Augsburg im Sinn, sondern etwa auch:

1. Den Lohwald in Meitingen. Dieser ist als Bannwald eigentlich
   geschützt; sein Boden ist nach einem Gutachten des BUND Naturschutz
   ökologisch besonders wertvoll ist. Er soll nun einem Stahlwerkausbau
   weichen. Alle umliegenden Gemeinden sind dagegen, drei
   Bürger\*inneninitiativen engagieren sich für den Erhalt des Lohwalds.
   Verantwortlicher für die Rodungspläne ist Max Aicher (86, Besitzer mehrerer
   Schlösser, einer der reichsten Menschen Deutschlands).

2. Das Lechtal im Augsburger Osten. Dieses wichtige Trinkwasser-, Naturschutz
   und Naherholungsgebiet soll durch die [Osttangente](https://www.youtube.com/watch?v=WIA7azJsyyo),
   eine vierspurige Autoschnellstraße, durchschnitten werden. Wir sagen: Wir
   benötigen keine neuen Autoschnellstraßen!

Da sich diese Lebensräume außerhalb der Stadtgrenzen befinden, entzieht sich
ihr Erhalt dem direkten Wirkungskreis der Stadt. Natürlich hat die Stadt aber
trotzdem einen großen politischen und verwalterischen Einfluss, den sie nutzen
kann und muss.


## 2. Energierevolution in Augsburg

### ❌ Augsburger Kohleausstieg bis 2023

Die Augsburger Stadtwerke beziehen nach wie vor Kohlestrom und heizen damit
jeden einzelnen Tag ganz unmittelbar die Klimakatastrophe an. Die Zerstörung
der Dörfer auf der Todesliste von RWE geht damit auch auf Augsburger Rechnung.

Zugleich ist ein hundertprozentiger Umstieg der Stadtwerke auf Ökostrom
vergleichsweise leicht zu bewerkstelligen, da die aktuellen Graustromverträge
nach Auskunft des Stadtwerke-Chefs nur eine Laufzeit von zwei Jahren haben und
eine Umstellung in einem ersten Schritt keinerlei Infrastrukturmaßnahmen
erfordert.

Der Stadtwerke-Chef gab auch an, dass die Stadtwerke nur vier Millionen Euro
jährlich benötigen würden, wenn sie eine solche Umstellung ohne Kostenfolge für
Augsburgs Bürger\*innen realisieren sollten. Für viele Jahre könnte die
Umstellung also alleine aus dem Verkauf der restlichen Augsburger Anteile an
Erdgas Schwaben finanziert werden. (Einige Anteile verkaufte die Stadt bereits
vor Jahren unter Oberbürgermeister Gribl -- ein guter Schritt! Details
[veröffentlichte](https://www.forumaugsburg.de/s_2kommunal/Kommunalpolitik/200704_stadtratssitzung-am-28-mai-5-gruendung-energiegesellschaft-mittlere-donau-erdgas-schwaben-swa/index.htm)
das Forum solidarisches und friedliches Augsburg.)

Im Übrigen fordern wir eine deutliche Neupositionierung der Stadtwerke, weg von
überregionaler Spekulation und hin zu einem verlässlichen Dienstleister für die
Region.


### ❌ Schnellerer Solar- und Windenergieaufbau

Für geeignete Flächen für Windkraftanlagen muss umgehend Baurecht geschaffen
werden. Nach Auskunft der Bürgerenergiegenossenschaft stünden sie sofort als
Investitionspartner zum Bau und Betrieb von Windkraftanlagen zur Verfügung.

Die Stadt muss bei ihren eigenen Gebäuden mit gutem Beispiel vorangehen: Von
denkmalgeschützten Gebäuden natürlich abgesehen, müssen diese mit
Photovoltaikanlagen versehen werden.

Und selbstverständlich dürfen wir unseren Altlastbestand nicht ausbauen:
Solarpflicht für Neubauten!


### ❌ Positionierung der Stadt gegen das Kohleeinstiegsgesetz vom 3. Juli 2020

Nach [Einschätzung von
Wissenschaftler\*innen](https://www.scientists4future.org/defizite-kohleausstiegsgesetz-kvbg-e/)
kann Deutschland das Pariser Klimaabkommen nicht einhalten, wenn -- wie jetzt
durch das Kohleeinstiegsgesetz vereinbart -- Kohle bis 2038 und unter
Nichtberücksichtigung selbst der enttäuschenden Ergebnisse der Kohlekommission
künstlich mit Steuermitteln am Leben erhalten wird.

Eine Rücknahme des Kohleeinstiegsgesetzes ist Sache des Bundestags, nicht der
Stadt. Wir fordern daher, dass sich die Stadt einerseits über die relevanten
überregionalen Gremien wie den Deutschen Städtetag dafür einsetzt. Zumindest
ein erster Schritt in diese Richtung unternahm Umweltreferent Reiner Erben mit
einem Brief an den Städtetag.

Zudem muss sich die Stadt aber öffentlichkeitswirksam gegen dieses Gesetz
aussprechen. Eine solche (rein symbolpolitische) Positionierung würde
Nachahmerstädte finden und so politischen Druck auf den Bundestag ausüben.

Das Kohleeinstiegsgesetz ist in den letzten Monaten im öffentlichen Diskurs in
Vergessenheit geraten. Dieser Umstand reduziert nicht die immense
Klimazerstörung, die von ihm ausgeht! Deutschland ist in Sachen Kohleförderung
nicht ein Land von vielen, sondern [Platz 1 weltweit](https://de.wikipedia.org/wiki/Kohle/Tabellen_und_Grafiken),
riesige Flächenstaaten wie USA, China und Russland eingerechnet.


## 3. Verkehrswende in Augsburg

### ❔ Fahrradstadt JETZT

Eine gute Nachricht! Möglicherweise wird die Stadt das [lokale
Radbegehren](https://www.fahrradstadt-jetzt.de/) bald annehmen.

Leider bleibt trotzdem Grund zur Vorsicht. Die Stadt versprach bereits 2012,
das damals groß angekündigte Projekt "Fahrradstadt 2020" ehrgeizig anzugehen.
Dieses Versprechen entpuppte sich als bewusste Täuschung. Die Stadt schob große
Versprechen vor, während sie tatsächlich nur an einem Minimalprogramm eines
Flickenteppichs als Radwegeprogramm interessiert war.


### ❌ Ausbau des ÖPNV

"Lasst das Auto stehen!" Ein solcher Ausspruch ist herablassend und
realitätsfern. Momentan sind viele Augsburger\*innen auf das Auto angewiesen,
da Bus und Tram viel zu wenig ausgebaut sind. Das muss sich ändern! Bisher ist
keine Änderung in Sicht.

Dazu gehört auch eine Prüfung des Verkehrskonzepts 4.0.


### ❌ Kostenfreier ÖPNV für alle

Einhergehend mit einem Ausbau des ÖPNV muss der ÖPNV massiv vergünstigt werden,
sodass ihn sich alle leisten können.


### ❌  Keine Osttangente

Der motorisierte Individualverkehr muss auf systematische Art und Weise
erheblich reduziert werden. Momentan haben die zuständigen Planer\*innen aber
immer noch die Vorschrift, mit einem gesteigerten Autoverkehr zu kalkulieren.
Das muss sich ändern!

Die schon oben erwähnte Osttangente ist ein aus der Zeit gefallenes Projekt.
Wir schätzen das langjährige Engagement des [Aktionsbündnis Keine
Osttangente](https://keine-osttangente.de/)!

Wir werden die Osttangente mit allen zur Verfügung stehenden Mitteln
verhindern, notfalls durch eine friedliche Besetzung nach Vorbild des Hambacher
Forsts und Dannenröder Walds. Wenn die Verantwortlichen schon nicht den
unbezifferbaren Wert der Ökosysteme einkalkulieren, dann vielleicht zumindest
die [nicht unerheblichen Polizeieinsatzkosten](https://klimaschutz.madeingermany.lol/)?


## Systemwandel statt Klimawandel

Für ein glückliches, sozial gleiches, klimagerechtes freies und gutes Leben für alle!


<!--
## Hinweis: Der Stadt ist nicht zu trauen

In einigen wenigen Bereichen setzte sich die Stadt Ziele, die mit unseren
übereinstimmen. Hier heißt es, besonders vorsichtig zu sein. Nach dem Verhalten
von Oberbürgermeisterin Weber in den Verhandlungen vor Weihnachten ([Details
hier](/pages/offeneBriefe/2021-02-03-Offene-Antwort.html)) verloren wir das
letzte Vertrauen in die Stadt, das wir bis dahin noch hatten.

Unter anderem folgende Dinge versprach die Stadt, ohne sie umzusetzen:

* Monatliches Dialogformat ([2020 versprochen](https://ratsinfo.augsburg.de/bi/vo020.asp?VOLFDNR=10900)) -- bislang nicht umgesetzt
* Fahrradstadt ([2012 versprochen](https://www.augsburg.de/fileadmin/user_upload/buergerservice_rathaus/verkehr/radverkehr/fahrradstadt-augsburg-grundsatzbeschluss.pdf)) -- ...
* Bio-Verpflegung auf städtischen Veranstaltungen (2007 versprochen) -- nie
  eingehalten
* Recyclingpapier in Stadtverwaltung, Schulen und Eigenbetrieben (2010
  versprochen) -- momentan belegt Augsburg aber unter allen bayerischen
  Großstädten mit einem Recyclingpapieranteil von nur 47,7% [den letzten
  Platz](https://www.nachhaltigkeit.augsburg.de/fileadmin/nachhaltigkeit/data/Indikatorenbl%C3%A4tter_Zukunftsleitlinien/Indikatorblatt_%C3%962.2_Papiereinkauf_und_Recyclingpapier.pdf)

**Die Stadt handelt mutwillig, systematisch und langfristig klima- und
zukunftszerstörend.**
-->
