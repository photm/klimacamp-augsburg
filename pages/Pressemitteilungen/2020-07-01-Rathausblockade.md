---
layout: page
title:  "01.07.2020: Regierung eskaliert komplett – Fridays for Future Augsburg reagiert – Blockade des Rathauses"
date:   2020-07-01 01:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: -1
---

*Pressemitteilung von Fridays for Future Augsburg am 1. Juli 2020*

# Regierung eskaliert komplett -- Fridays for Future Augsburg reagiert -- Blockade des Rathauses

Nach bundesweiten Aktionen seitens Fridays for Future, Greenpeace, BUND
Naturschutz, Ende Gelände, Extinction Rebellion und anderen
Klimagerechtigkeitsinitiativen zeigt unsere Regierung immer noch keine
Anzeichen dafür, ihren Entwurf für das Kohleeinstiegsgesetz
zurückzunehmen.

Dieses Gesetz verhindert die Zukunft der jungen Generation. Es sorgt
dafür, dass Deutschland massiv über 1,5 °C hinausschießt und sich weiter
als Europas größter Verschmutzer profiliert. Wir fordern, dass sich die
Fraktionen im Stadtrat öffentlich und mit Nachdruck gegen diese
Verantwortungslosigkeit aussprechen.

Wir errichten daher am morgigen Mittwochabend um 19:00 Uhr eine
friedliche Rathausblockade. Wir gehen wieder, sobald es die Situation
zulässt. Nach einer Civey-Umfrage finden nur 13,1 % aller Deutschen es
eindeutig richtig, dass die Betreiber von Kohlekraftwerken 4,4 Mrd. Euro
Steuergelder erhalten sollen, und deutlich mehr als die Hälfte der
Deutschen findet es falsch. Die Regierung stellt mit ihrem Gesetzentwurf
die Interessen von einzelnen Kohlekonzernchef\*innen vor die ihrer
Arbeiter\*innen.

Die Entscheidung zur Blockade fiel nach den Fehlentwicklungen des
gestrigen Tages. Trotz der Kürze der Zeit werden wir ein Workshop- und
Filmprogramm aufstellen. Alle Interessierten sind aufgerufen, sich uns
anzuschließen. Isomatte, Schlafsack und Mund-/Nasenschutz bitte selbst
mitbringen.

Seit Beginn der Industrialisierung gerechnet, ist Deutschland der
viertgrößte CO₂-Emittent. Nur die USA, China und Russland tragen noch
mehr zur Erderhitzung bei. Fridays for Future fordert die Einhaltung des
2015 beschlossenen Pariser Klimaschutzabkommens. Zahlreiche deutsche
Wissenschaftler\*innen gaben eine sachliche Einschätzung des
Kohleeinstiegsgesetzes ab. Sie ist online unter
https://www.scientists4future.org/defizite-kohleausstiegsgesetz-kvbg-e/
zu finden.
