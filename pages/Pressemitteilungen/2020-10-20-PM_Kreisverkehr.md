---
layout: page
title:  "20.10.2020: Fahrradstadt: Wo bleiben 2020 die Volltreffer?"
date:   2020-10-20 23:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 23
---

*Gemeinsame Pressemitteilung vom Augsburger Klimacamp, ADFC, Radentscheid,
Forum Augsburg lebenswert und Bürgeraktion Pfersee am 20. Oktober 2020*

# Fahrradstadt: Wo bleiben 2020 die Volltreffer?

Das Klimacamp macht zusammen mit dem ADFC, dem Radentscheid, dem Forum 
Augsburg lebenswert und der Bürgeraktion Pfersee mit einer Demonstration 
auf Versäumnisse und Schwächen bei der Umsetzung der Fahrradstadt in 
diesem Jahr aufmerksam.

Die Aktion startet am Donnerstag den 22.10. um 15:15 an der Kreuzung der 
Stadionstraße, im weiteren Verlauf Perzheimstraße, mit der 
Schießstättenstraße. Die Polizei sperrt die Kreuzung von 15:30 bis 15:40 
komplett ab. Wir bauen symbolisch einen Kreisverkehr auf.


## Hintergrund

Unter dem Titel "Fahrradstadt 2020" schickte sich Augsburg 2012 an, in
die Championsleague des umweltfreundlichen Verkehrs aufzusteigen. Auf
halber Strecke ging die Puste aus und man strich kurzerhand die
Jahreszahl aus dem Ziel: Fahrradstadt irgendwann. Dennoch bietet die
symbolträchtige Jahreszahl einen guten Anlass, die Chancenverwertung in
diesem Jahr auf den Prüfstand zu stellen.

Nach langer - jahrzehntelanger - Vorarbeit durch zahlreiche
zivilgesellschaftliche Organisationen entschloss sich die Stadt zu einer
Aktion am als Angströhre berüchtigten Pferseer Tunnel: Seit dem Frühjahr
signalisieren dort zwei Schilder Tempo 30. Grund zu uneingeschränktem
Jubel? "Leider nein", urteilt Petra Kammerer von der Bürgeraktion
Pfersee, "es wirkt auf mich so, als hätte man nur das Allernötigste
getan. Warum keine deutlichere Kennzeichnung, etwa mit einem farbigen
Hinweis auf der Fahrbahn oder Smileys, die die tatsächliche
Geschwindigkeit anzeigen. Warum beschränkt sich das radfreundliche Tempo
auf den Tunnel? Im weiteren Verlauf bis zur Luitpoldbrücke passieren zu
viele Unfälle, da muss auch Tempo 30 her. Auch die parkenden Autos
machen es gefährlicher. Ich fordere Nachbesserungen".

Beim Ausbau der Infrastruktur für Autos spielt Augsburg durchaus bei den
Großen mit. So bekam die Stadionstraße vom TVA bis zur
Schießstättenstraße im Hochsommer einen neuen Belag. Die Strecke wurde
für einige Wochen gesperrt und die Baufahrzeuge rückten an. Eine gute
Vorlage um auch etwas für den emmissionsfreien Verkehr zu tun und den
geplanten Kreisverkehr an der Kreuzung zur Schießstättenstraße und
Perzheimstraße umzusetzen. Verwandelt? Leider nein. "Wir vom ADFC hätten
es so gerne gesehen, wenn man wenigstens eine der geplanten Radachsen
endlich durchgehend fertig gestellt hätte. Der Kreisverkehr an dieser
Stelle würde eine wichtige Lücke in der Achse III schließen. Wir können
nicht nachvollziehen, warum dies bis zur Umsetzung der Linie 5 warten
soll." so Almut Schwenke vom ADFC-Vorstand und vom Bürgerbegehren
"Fahrradstadt jetzt".

Immerhin ein Abstauber gelang bei der Ertüchtigung der Stadionstraße: Es
gibt jetzt Schutzstreifen für Radfahrer auf beiden Seiten. Endlich ein
Volltreffer? Wieder nicht. "Ich finde es gut, dass sich jetzt mehr Leute
mit dem Rad auf diesen Straßenabschnitt trauen. Die Autos fahren
langsamer" beobachtet Ingrid Schaletzky, die sich im Forum Augsburg
lebenswert und im Seniorenbeirat engagiert. "Aber bei der Verkehrsinsel
sehe ich ein großes Gefahrenpotential. Da endet der Schutzstreifen
einfach unvermittelt. Wenn hier Autofahrer, die sich nicht auskennen,
auf unsichere Radfahrende stoßen, kann es leicht zu brenzligen
Situationen kommen. Gerade für die ganz Kleinen und die reiferen
Jahrgänge bedeutet das Rad ein wichtiges Stück Freiheit. Das setzt eine
sichere Infrastruktur voraus. Und diese hat hier eine gefährliche
Lücke".

Das Klimacamp spielt bundesweit in der ersten Liga. Als erstes seiner
Art hat es Aktivist\*innen in vielen anderen Städten inspiriert, auf
diese Weise auf klimafreuliches Handeln zu drängen. "Den konsequenten
Umstieg aufs Rad und auf öffentliche Verkehrsmittel sehe ich als
schnellste und effektivste Möglichkeit an, wie Augsburg seinen Teil zur
Erreichung der vereinbarten Klimaziele beitragen kann", so Janika
Pondorf vom Klimacamp. "Warum läuft das alles so halbherzig? Warum immer
wieder die faulen Kompromisse mit den CO2-Schleudern? Bitte, bitte eine
Fahrradstadt jetzt, aus einem Guss, die den Namen verdient."


## Nachschau

**[Fotogalerie](https://www.speicherleck.de/iblech/stuff/kreiselaktion-2020-10-22/)**

Unserem Aufruf zur Teilnahme folgten auch die Stadträt\*innen: Anna Rasehorn
(SPD) und Deniz Anan (Grüne). Die Stadt reagierte auf unsere Aktionsankündigung
mit einer
[Pressemitteilung](https://www.speicherleck.de/iblech/stuff/kreiselaktion-2020-10-22/reaktion-der-stadt.pdf).
Wir sagen dazu:

Wir begrüßen, dass bei der Fußgängerinsel in der Stadionstraße noch für
die Sicherheit des Radverkehrs nachgebessert werden soll.

Wir können allerdings nicht nachvollziehen, wieso die Sicherung mit
Tempo 30 nicht sofort mit der Freigabe des Streckenabschnittes erfolgte. Das
Aufstellen eine provisorischen Schildes für eine Übergangsperiode erfordert
keine eigenen Haushaltsmittel.

Der Verweis auf fehlende Haushaltsmittel bestätigt genau den Anlass für
unseren Pop-up-Kreisverkehr: eine Priorisierung der Verkehrsträger, bei der
das Auto weiter an erster Stelle steht. Das Geld für die Erneuerung der
Fahrbahn über mehrere 100 Meter stand eben schon zur Verfügung.

Auch die Linie, bei der Umsetzung von Verbesserungen im Radverkehr auf
die Fertigstellung von Projekten für andere Verkehrsmittel zu warten,
halten wir für nicht mehr zeitgemäß. Sichere Verhältnisse für den
Radverkehr in der Pferseer Straße müssen nicht auf die Linie 5 warten,
Tempo 30 und der Abbau von Parkplätzen kann umgehend erfolgen.

Es steht nie in der Absicht von uns Aktivist\*innen, die Arbeitsleistung
im Tiefbauamt herabzuwürdigen. Wir halten die bisherigen Umsetzungen im
Jahr 2020 im Tempo und in der Ausgestaltung nicht für ausreichend um in
absehbarer Zeit zu einer echten Fahrradstadt zu kommen. Wir haben an
konkreten Beispielen gezeigt, wo die Augsburger\*innen bei konsequenter
Umsetzung der Forderungen der Bürger\*innenbegehrens "Fahrradstadt jetzt"
bessere Lösungen erwarten können. Selbstverständlich erfordert das auch
eine angemessene Ausstattung mit Stellen und Haushaltsmitteln.
