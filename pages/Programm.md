---
layout: page
title: Programm & Tagebuch
permalink: /programm/
nav_order: 2
---

# Programm
Hier findest du unser Workshop- und Vortragsprogramm im Klimacamp.
Daneben gibt es alle 12 Stunden ein Plenum zur Camporganisation und mehrmals am Tag kurze Redebeiträge.

*Wir gestalten das Camp alle gemeinsam. Deine Workshopidee ist willkommen!*

**Hinweis: Da Augsburg sich leider zum Corona-Hotspot entwickelt hat, haben wir freiwillig Mitte-Ende
November drastisch unser Programm reduziert. Wir hoffen, dass sich die Lage bald bessert und wir wieder
eine Vielfalt an Bildungs-, Aktions- und Kreativprogramm rund um die Klimagerechtigkeit bieten können.**
Nicht alle Veranstaltungen sind hier eingetragen. Die beste Möglichkeit, auf dem aktuellen Stand zu
bleiben, ist unsere [Telegram-Gruppe](https://t.me/joinchat/OG0nMhgCvrN8zvU9cV61JA).

## Freitag, 19.03.
### `Uhrzeit folgt` Globalstreik von Fridays for Future
Der 19. März wird fett! Seit über 2 Jahren streiken wir für mehr Klimagerechtigkeit. Wir fordern nicht mehr, als dass die Politik ihre eigenen Versprechen, das 1,5-Grad Ziel, einhält. Der Globalstreik steht deshalb unter den Hashtags #NoMoreEmptyPromises und #AlleFür1Komma5.

Mehr Infos [hier](https://www.fff-augsburg.de/nomoreemptypromises/).

## Sonntag 28.2.
### `14:00 Uhr` Demo zum Erhalt des Lohwalds in Meitingen

<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/5.jpeg" style="width: 100%">

Einer der reichsten Menschen Deutschlands möchte einen besonders geschützten
Bannwald roden, um sein Kohlestrom-betriebenes Stahlwerk zu vergrößern. Zuvor
baute er in die andere Richtung einen Parkplatz hin, sodass es jetzt "keine
andere Möglichkeit" gibt. Laut BUND Naturschutz ist insbesondere der Boden des
Bannwalds ökologisch unglaublich wertvoll. Alle umliegenden Gemeinden sind
gegen diesen Irrsinn.

Der Mann, um den es geht, ist Max Aicher, 86 Jahre, Besitzer mehrerer
Schlösser, und der Bannwald ist direkt bei uns vor Ort. Es ist der Lohwald in
Meitingen.

Es gibt gleich drei Bürger\*inneninitiativen, die gegen dieses aus der Zeit
gefallene Projekt kämpfen. Ein breites Bündnis ruft daher zur Demo diesen
Sonntag auf, darunter Klimacamp und Fridays for Future Augsburg. Meitingen ist
klein, da kann eine Demo viel bewirken! Kommt unbedingt vorbei! Bitte teilt
diesen Aufruf!

Falls ihr nicht an unserer Zubringerdemo teilnehmen möchte: direkter Treffpunkt
"Am Lohwald" südlich der Lechstahlwerke.

## Freitag 26.2.
### `14:00 Uhr` Lerne aktivistisches Klettern!

<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/1.jpeg" style="width: 30%">
<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/2.jpeg" style="width: 30%">
<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/3.jpeg" style="width: 30%">

## Donnerstag 25.2.
### `16:00 Uhr` Interview mit Salzburger Zeitung

### `18:00 Uhr` Online-Workshop: Hoffnung durch Handeln (Dr. Tobias Bayr, Klimaforscher am GEOMAR Kiel)

<img src="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/4.jpeg" style="width: 100%">

Wie kann mensch es schaffen, sich mit den globalen Themen wie Klimaerwärmung,
ökologische Krise und soziale Ungerechtigkeit zu beschäftigen, ohne sich von
der Größe der Themen überwältigen zu lassen? In diesem Workshop erhältst du
„Werkzeug“, das dir dabei helfen kann. Dies kann uns helfen, aus der Ohnmacht
ins Handeln zu kommen und sich kraftvoll für unseren Planeten einzusetzen.

[Dr. Tobias Bayr](http://www.klimafrosch.de/) wurde 1979 geboren, studierte
Meteorologie und arbeitet als Klimaforscher am GEOMAR in Kiel. Von 2014 bis
2015 lebte er im Ökodorf ZEGG. Er ist ein leidenschaftlicher „Change Maker“,
der von Tiefenökologie, Buddhismus und gewaltfreier Kommunikation inspiriert
ist.

Einladungslink: [via Zoom](https://uni-kiel.zoom.us/j/83022867468?pwd=dmVneERLZnFwbE5jNm9udm5FWjJuZz09)
(Meeting-ID: 830 2286 7468, Kenncode: 996563)

## Samstag 13.3.
### `11:00 Uhr` [Baum-Demo](https://baumallianz-augsburg.de/archive/1944)

## Freitag 5.2.
### `5:45 Uhr` Lehrpfaderrichtung im Augsburger Stadtwald

## Mittwoch 3.2.
### `13:00 Uhr` Interview mit ARD
### `17:00 Uhr` Demonstration mit Gehzeugen in der Hermanstraße

## Montag 1.2.
### `6:00 Uhr` Baumbesetzung in Gersthofen beim Paul-Klee-Gymnasium

## Mittwoch 27.1.
### `7:00 Uhr` Interview mit BR (den gesamten Vormittag über)

## Montag 25.1.
### `9:30 Uhr` Treffen mit BR
### `12:00 Uhr` Interview mit Radiosender M94.5

## Samstag 2.1.
### `11:30 Uhr` Pressekonferenz

## Samstag 12.12.
### `18:00 Uhr` Fünf Jahre Klimaabkommen von Paris (von XR organisiert, Treffpunkt 17:15 Uhr)

## Donnerstag 26.11.
### `18:00 Uhr` Bericht aus dem Danni

## Montag 16.11.
### `15:00 Uhr` Eine Schulklasse besichtigt das Klimacamp

## Mittwoch 4.11.
### `16:00 Uhr` Gespräch mit der neuen Sprecherin der Grünen Augsburg

## Freitag 30.10.
### `16:00 Uhr` Demo: Die Klimakrise is gruseliger als jeder Horrorfilm

## Mittwoch 28.10.
### `18:30 Uhr` Martin Kleineidam (Pastor, Dreifaltigkeitskirche Augsburg): Die fünf Transformationskräfte der Energiewende – jeder und jede kann mitmachen – jederzeit

**Bitte kommt nur, wenn ihr bei der Anreise kein Risiko eingehen müsst (also wenn ihr zum Beispiel nicht mit öffentlichen Verkehrsmitteln kommen müsst). Der Vortrag wird aufgezeichnet, in der folgenden Nacht auf YouTube hochgeladen und dann an dieser Stelle verlinkt.**

Vortrag auf Youtube: https://www.youtube.com/watch?v=_tK2TJmY7PY

## Montag 26.10.
### `11:30 Uhr` Inhaltliches Gespräch mit Vertreter\*innen der FDP Augsburg [VERSCHOBEN!]

## Samstag 24.10.
### `14:30 Uhr` Schienenaktion auf dem Rathausplatz -- für den Erhalt und
Ausbau von Gütergleisen

Die Deutsche Bahn legt Gütergleise am Augsburger Bahnhof still...

## Donnerstag 22.10.
### `15:30 Uhr` Kreiselaktion bei der Kreuzung Stadionstraße/Schießstettenstraße

Wir errichten einen künstlichen Kreisverkehr! Zusammen mit FAL, BAPS und
Radentscheid Augsburg.

### `19:00 Uhr` Vorstellung von Transition Town Augsburg

Wie können wir schonend mit Ressourcen umgehen? Wie unsere regionale
Selbstversorgung stärken? Einblick in eine Bewegung mit über 2500 Ortsgruppen

## Mittwoch 21.10.
### `17:00 Uhr` Gemeinsamer Besuch der Moria-Demo

## Sonntag 18.10.
### `17:00 Uhr` Besuch von Lisa Badum: Bundestagsabgeordnete, klimapolitische Sprecherin der Grünen, bürger\*innennahe Energiewende, Entkopplung von Wirtschaftswachstum und TreibhausgasenEntkopplung von Wirtschaftswachstum und Treibhausgasen, CO2-Preis, ...

## Samstag 17.10.
### `14:00 Uhr` Gemeinsam Rechte einschränken (leider abgesagt)
### `14:00 Uhr` Beitrag zur Stadtrallye der CAJ Augsburg

## Samstag 10.10. -- Wir begehen unser 100-tägiges Bestehen (begleitet von BR und Channel Welcome/FILMUP!)
### `14:30 Uhr` Diskussionsrunde mit Lokalpolitiker\*innen: CSU, SPD, DIE LINKE, Grüne, FDP, FW, ÖDP, AiB, Die PARTEI
### `16:30 Uhr` Online-Teilnahme an der [Klimakonferenz LCOY](https://www.lcoy.de/youtube): Geht Klima lokal?
### `17:30 Uhr` Lesung: [Land unter](http://www.spbonline.de/#landunter), ein Climate-Fiction-Roman von Dieter Rieken
### `18:30 Uhr` Erfahrungen aus 40 Jahren Anti-Atomkraft-Bewegung aus gewaltfrei-anarchistischer Sicht
### `20:00 Uhr` Leckeres warmes veganes Abendessen
### `21:00 Uhr` [Baumbesetzung](/pages/Pressemitteilungen/2020-10-08-Trauerweide.html)

## Montag 05.10.
### `18:30 Uhr` Gespräch mit einer Stadträtin der CSU-Fraktion

## Freitag 02.10.
### `19:00 Uhr` Ende Gelände: Wie war es? Erzählungen, Erfahrungsaustausch, Reflektion

## Donnerstag 01.10.
### `10:15 Uhr` (vormittags) Gespräch über umweltpolitische Bildung und Umweltpolitik
mit Simone Strohmayr (SPD-Landtagsabgeordnete für Aichach-Friedberg, Bildungspolitische Sprecherin der SPD-Fraktion) und Florian von Brunn (SPD-Landtagsabgeordneter für München-Giesing, Sprecher für Umwelt- und Verbraucherschutz)
### `20:00 Uhr` Campkino

## Mittwoch 30.09.
### `19:00 Uhr` "Climate Connect" -- Info über eine neue Austauschplattform für Aktivisti

### `19:30 Uhr` Bericht von Stephan Kreppold (Biobauer, Sprecher der Regionalgruppe Bayerisch-Schwaben der [AbL](https://www.abl-ev.de/start/))

Was ist der aktuelle Stand, wie geht es unseren Bauern? Wie gehen
Landwirtschaft und Klimaschutz Hand in Hand? Wo gibt es Reibungsstellen?
Wo hilft die Landesregierung, wo legt sie Steine in den Weg?

## Dienstag 29.09.
### `19:00 Uhr` Fairer Handel und Postwachstum

## Samstag/Sonntag 26./27.09.

Wir sind fast alle beim Ende-Gelände-Aktionswochenende und blockieren friedlich
mit unseren Körpern Kohleinfrastruktur.

## Freitag 25.09. -- **Globaler Klimastreik**
### `17:00 Uhr` Demonstrationen (Zu-Fuß-Geh-Demo und Fahrrad-Demo)
Startpunkt für beide: Ulrichsplatz
### im Anschluss: Gemeinsame Fahrt zum Ende-Gelände-Aktionswochenende!

## Donnerstag 24.09.
### `16:00 Uhr` Do-it-yourself: T-Shirts/Stoff/Holz bedrucken

Am besten eigenes T-Shirt mitbringen! (gut ist weiß)

### `19:00 Uhr` Utopismus -- Wege zu einer anarchistischeren Gesellschaft

Der Workshop schließt frei an die Gesprächsrunde mit Jörg Bergstedt an, ohne
dass die dort besprochenen Themen eine Voraussetzung wären. Ich würde gerne in
einer offenen Diskussionsrunde mit allen die sich interessieren gemeinsam über
Konzepte und Vorstellungen zu "Utopismus", "utopischem Handeln" und den
Möglichkeiten einer anarchistischeren/weniger durch Macht strukturierten
Gesellschaft sprechen. Natürlich würde ich mich auch sehr darüber freuen,
einige der Gesichter wiederzusehen, die an meinem ersten Workshop vor etwas
mehr als zwei Monaten teilgenommen haben, in dem wir über Ideologie und politische
Theorie sprachen.

## Mittwoch 23.09. -- **Tag 85**
### `16:30 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren
### `18:00 Uhr` Bezugsgruppenfindung für Ende Gelände
### `19:00 Uhr` Das Machbare umsetzen: Schritte zur sozial-ökologischen Transformation -- Vortrag von Jörg Alt (Jesuitenorden)

„Intellektuelle Dekolonisierung“ -- Was zählt im Leben? --
Die Gegennarrative zum Neoliberalismus? -- Arbeiten mit dem, was wir haben:
Bayernplan plus Druck der Straße

## Dienstag 22.09.
### `18:00 Uhr` Gesprächsrunde mit dem Leiter des Schwerpunkts Klima und Energie am LfU
### `20:00 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren
### `20:00 Uhr` Anzüge bemalen für Ende Gelände

## Montag 21.09.
### `17:15 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren
### `20:30 Uhr` Campkino: [Die Freiheit, die wir hier erlebten... – Leben im Dannenröder Forst](https://www.youtube.com/watch?v=ENDjmnbMJaY)

## Sonntag 20.09.
### `14:00 Uhr` [Kidical Mass: Die bunte Fahrraddemo für alle](/kidical-mass/)
### `19:00 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren

## Samstag 19.09.
### `16:00 Uhr` Crashkurs Kernenergie und wieso sie keine Lösung für die Klimakrise ist

## Freitag 18.09. -- **Tag 80**
### `15:00 Uhr` Parking Day
### `19:00 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren

## Donnerstag 17.09.
### `14:00 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren

### `18:00 Uhr` Bericht aus der Geothermiebranche

An welchen Stellen werden Firmen in der Geothermiebranche Steine in den Weg gelegt? Was müsste
passieren, damit sie ihrer Arbeit besser nachgehen könnten? Wie hat sich die
Branche in den vergangenen Jahren verändert? Gibt es politische Entscheidungen,
die die Branche sehr begrüßt oder sehr bedauert? Wie reagiert die Öffentlichkeit auf Projekte?

### `19:30 Uhr` Gesprächsrunde zur emotionalen Verarbeitung der Klimakrise (von zwei Psychologists for Future)

## Mittwoch 16.09.
### `18:00 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren

## Dienstag 15.09.
### `20:00 Uhr` Vortrag von Robert Kugler (Naturwissenschaftlicher Verein Schwaben) über Artenschutz

## Montag 14.09.
### `18:00 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren

Am 25.9. gehen wir mit FFF auf die Straßen und appellieren zum ersten Mal seit
Corona im großen Stil an unsere Regierung, die Klimakrise endlich ernst zu
nehmen! 📣 Und direkt im Anschluss nehmen wir den Kohleausstieg symbolisch
selbst in die Hand, denn am Wochenende 25./26./27.9. heißt es: Auf geht's, ab
geht's, Ende Gelände! 💪 Wir blockieren friedlich mit unseren Körpern
Kohleinfrastruktur wie Kohlegruben, Kohlebagger oder Kohleschienen. 🏭

Damit sich auf dem EG-Aktionswochenende auch alle Neulinge, die noch nie
zivilen Ungehorsam leisteten, wohlfühlen und voll einbringen können, finden in
allen größeren Städten, und so auch in Augsburg, zahlreiche Aktionstrainings
und Infoveranstaltungen zur ausführlichen Vorbereitung statt. Auch die Anreise
wird so solidarisch organisiert. Schaut unverbindlich vorbei!

Nehmt den frühest für euch möglichen Termin wahr, damit ihr euch lange und in
Ruhe einstimmen könnt! Nehmt zu den Veranstaltungen gleich Geschwister, Eltern
und Freund\*innen mit! (später wird es auch Aktionstrainings zur Rebellion Wave von XR geben)


## Samstag 12.09.
### `11:00 Uhr` Trauermarsch von XR

### `17:00 Uhr` Evakuiert Moria! - Kundgebung am Elias-Holl-Platz

### `18:00 Uhr` Wie geht Mobilisierung für Klimagerechtigkeit (Mobikonzept)

### `21:00 Uhr` Campkino - mit Videos von Harald Lesch und anderen Wissenschaftler\*innen

## Freitag 11.09.
### `7:00 Uhr` Große Aktion: Wir erobern uns die Straße zurück.

Treffpunkt 6:50 Uhr bei der Apotheke am Königsplatz. Wir wandeln eine Fahrspur
der Hermanstraße stadteinwärts in einen Radweg -- und zwar bis fast hinter zur
Brücke!

### `18:00 Uhr` Informationsveranstaltung zum Ende-Gelände-Aktionswochenende -- friedlich Kohleinfrastruktur blockieren

### `20:00 Uhr` Klimaschutz beginnt beim Papier (von Ute Michallik vom Arbeitskreis Papierwende der Lokalen Agenda)

## Donnerstag 10.09.
### `14:00 Uhr` Gesprächsrunde mit Max Weinkamm (CSU)
### `17:00 Uhr` Gesprächsrunde mit dem Radbeauftragten der Stadt und dem Leiter des Tiefbauamts
### `19:00 Uhr` Fließendes Geld -- Was ist Geld? Der Konstruktionsfehler im Geldsystem? Wirkungsmöglichkeiten mit unserem Geld? Alternativen zu Geld? Zusammenhang Geld und Klima?

## Mittwoch 09.09.
### `12:00 Uhr` Aktionstraining Ende Gelände
### `20:00 Uhr` evtl. Filmvorführung (evtl. zu Klimagerechtigkeit und Ernährung)

## Dienstag 08.09. -- **Tag 70**
### `11:30 Uhr` Wir machen die Hallstraße zur Spielstraße!

Treffpunkt 11:20 Uhr direkt vor dem Holbein-Gymnasium.

## Montag 07.09.
### `20:00 Uhr` Intelligente Stromnetze
### `21:00 Uhr` Filmvorführung: Hoch- und Tiefbau im Hambi

## Sonntag 06.09.
### `16:30 Uhr` Aktionstraining Ende Gelände

## Samstag 05.09.
### `20:30 Uhr` Filmvorführung: [Beyond the red lines](http://beyondtheredlines.org/de/trailer/)

## Freitag 04.09.
### `18:30 Uhr` Demozug für Klimagerechtigkeit
### `20:00 Uhr` Was kosten uns Lebensmittel wirklich? (Dr. Tobias Gaugler, Amelie Michalke, Universität Greifswald)

*Beginn: Am Klimacamp*

## Donnerstag 03.09. -- **Tag 65**
### `16:00 Uhr` Der [Tauschring](http://lets-augsburg.de/) stellt sich vor -- Herausforderungen und Erfolge
### `17:00 Uhr` Die Anti-Kohle-Kidz stellen sich vor -- friedlich Kohleinfrastrutur blockieren
### `20:30 Uhr` Campkino: [Ende Gelände -- Der Film](https://www.youtube.com/watch?v=w8mtbITzMic)

## Mittwoch 02.09.
### `18:00 Uhr` Digitale Selbstverteidigung für Klimagerechtigkeitsaktivist\*innen
### `19:30 Uhr` Waldkindergärten und Klimagerechtigkeit
### `20:30 Uhr` Wissenschaftlicher Vortrag: Fluchtursache Klimawandel? (Dr. Matthias Schmidt, Lehrstuhlinhaber Humangeographie und Transformationsforschung)

## Dienstag 01.09.
### `19:00 Uhr` Gemeinsame Reflektionsrunde zu den ersten 60 Tages des Camps

## Montag 31.08.
### `20:30 Uhr` Über die Verbindung von Internetsicherheit und Klimagerechtigkeit

## Donnerstag 27.8.
### `17 Uhr` Intelligentes Stromnetz
### `21 Uhr` Campkino

##  Mittwoch 26.8.
### `17 Uhr` Besuch von Jugenddeligierten der UN
### `18 Uhr` Diskussionsrunde mit der Bürger-Energie-Genossenschaft Neuburg-Schrobenhausen-Aichach-Eichstätt eG und Bruno Marcon

## Dienstag 25.8.

### `10 Uhr` Anarchismus - ein Update ist nötig und möglich!

*Wie lässt sich herrschaftsfreies Utopie schon heute im Kleinen umsetzen?*  
*Workshop mit Jörg Bergstedt*

Zunächst geht es um Anforderungen an herrschaftsfreie Gesellschaftsentwürfe und Utopien (siehe oben: „Freie Menschen in freien Vereinbarungen“). Danach werden anarchistische Ansätze darauf untersucht, wieweit sie tatsächlich emanzipatorische bzw. herrschaftsfreie Perspektiven bieten. Am Ende steht ein Ausblick, wie sich anarchistische Theorie und Praxis weiterentwickeln müsste: Moderne Herrschaftsanalysen, ein wissenschaftlicher, materialistischer Anarchismus (Leben ohne Jenseits, Übernatürlichkeit und Determinismus) und Vorschläge für neue Ansätze, Theorien und praktische Konzepte.


## Montag 24.8.

### `10 Uhr` Direct-Action-Training (Teil 2)
*Mit Jörg Bergstedt*

Du findest, in der Welt läuft einiges verkehrt? Und fühlst Dich ohnmächtig, weil Du oft nicht weißt, wie das Bessere gelingen oder durchgesetzt werden kann? Umweltzerstörung, Menschenrechtsverletzungen in Zwangsanstalten, Diskriminierung und Ausbeutung - so vieles passiert täglich, aber kaum etwas hilft dagegen? Dann hilft dieses Direct-Action-Training. Denn so ohnmächtig, wie es scheint, sind wir nicht. Im Gegenteil: Es gibt viele Aktionsformen, die wir kennenlernen und üben können, um uns wirksamer wehren zu können, um lauter und deutlicher unsere Stimme zu erheben oder uns politisch einzumischen: Kommunikationsguerilla, verstecktes Theater, gezielte Blockaden oder Besetzungen, intelligente Störung von Abläufen und vieles mehr schaffen Aufmerksamkeit und bieten Platz für eigene Forderungen und Visionen. Wir werden konkrete Aktionsideen besprechen, den rechtlichen Rahmen durchleuchten und einiges ausprobieren.

## Sonntag 23.8.

### `10 Uhr` Direct-Action-Training (Teil 1)
*Beschreibung siehe 24.08.*

## Samstag 22.8.

### `15 Uhr` Workshop „Macht macht Umwelt kaputt - über den Zusammenhang von Herrschaft und Umweltzerstörung“
*mit Jörg Bergstedt*

Herrschaft bedeutet die Möglichkeit, Abläufe und Verhältnisse so regeln zu können, dass andere die negativen Folgen erleiden müssen. Umweltzerstörung basiert regelmäßig auf diesem Prinzip: Industrie und ihre Staaten graben in armen Regionen nach Energiequellen und Rohstoffen, transportieren schiffeweise Nahrungsmittel oder Holz zu sich und kippen den Müll wieder in die Peripherien zurück. Städte nutzen das Umland für Bauflächen, Straßentrassen und Müllhalden. Die Natur zählt nichts, weil die Menschen in ihr still sind oder still gehalten werden. Wer Umwelt dauerhaft schützen will, muss daher die Machtfrage stellen. Doch was geschieht tatsächlich? Selbst Umweltverbände setzen auf Staat, Umweltpolizei, Gesetze und Firmen, um die Welt grün zu halten. Diese Schüsse gehen nach hinten los - schon seit Jahrzehnten. Nötig ist eine Umweltschutzstrategie, die die Menschen ermächtigt, ihr Leben wieder selbst zu organisieren – ohne Hierarchien und Privilegien. Nur ein Umweltschutz von unten ist grundlegend und dauerhaft wirksam.

### `19 Uhr` Workshop „Freie Menschen in freien Vereinbarungen“
*mit Jörg Bergstedt*

Wie kann eine herrschaftsfreie Welt aussehen? Diese Frage beschäftigt PhilosophInnen, manch zukunftsorientierten PolitikerInnen oder AktivistInnen, Roman- und Sachbuchschreiberlinge. Doch ein kritischer Blick zeigt meist: Zukunftsdebatten sind eher ein Abklatsch heutiger Bedingungen mit netteren Menschen in der Führung. „Freie Menschen in freien Vereinbarungen“ ist radikal anders: Mit scharfem, analytischen Blick werden die Bedingungen seziert, unter denen Herrschaft entsteht, wie sie wirkt und was sich wie ändern muss, damit Menschen aus ihrem Streben nach einem besseren Leben (Eigennutz) sich nicht nur selbst entfalten, sondern genau dafür die Selbstentfaltung aller Anderen brauchen und deshalb mit herbeiführen. Aus Konkurrenz wird Kooperation, das Normale weicht der Autonomie.

### `21 Uhr` Dannenröder Wald
Vortrag über den Dannenröder Wald, von einem dort lebenden Aktivisten.

## Freitag 21.8. -- **Tag 50 🎉🥳😱**

### `16:50 Uhr` Soli-Aktion für die Aktivist\*innen im Dannenröder Wald

### `21:30 Uhr` Film Direct Action - Kunst des kreativen Widerstands
„Direct Action“ ist eine Form kreativen Widerstandes, die wir als Teil gesellschaftlicher Intervention gegen Herrschaft und Verwertung sowie als Eröffnung von Diskussionen um visionäre, emanzipatorische Gesellschaftsformen verstehen. Sie versteht sich als gleichberechtigter Teil zu anderen kreativ-emanzipatorischen Handungsstrategien wie Gegenöffentlichkeit, Freiräume und Aneignung, versucht aber, Erstarrungen in den Aktionsformen und -strategien zu überwinden, z.B. die Wirkungslosigkeit vieler vereinheitlichender Aktionsformen (Latschdemo, Lichterkette …) oder das Gegeneinander aufgrund verschiedener Aktions- und Ausdrucksformen.
„Direkte Aktion“ ist mehr als nur mal hier eine Blockade oder da ein Steinwurf. Sie ist eine Methode, ein Aktionskonzept und eine Idee für eine Politikform, die nicht mehr nur Einzelnes angreift – aber auch mehr will als schwächliche Miniveränderungen innerhalb von umweltzerstörenden und menschenverachtenden Verwertungs- und Herrschaftsstrukturen.

## Donnerstag 20.08

### `20:30 Uhr` Von besetzten Betrieben und ökologischer Seife
Die Seiferei stellt sich vor und bringt Seife mit.

## Mittwoch 19.08 
### `15:00` Demotechnik Workshop

### `20:00` Keine Osttangente Augsburg

Vermutlich Vortrag 'Keine Osttangente Augsburg'.
Über den laufenden Aktivismus und Bürgerprotest gegen eine geplante 'autobahnähnliche' Straße (sowas wie eine neue B17 für Augsburg).

## Dienstag 18.8.
### `21:30` Vortrag: „Einfluss des Klimawandels auf Flora, Fauna und Lebensräume im Allgäu“
*Evt. erst um 22:30.* Vortrag vom international renommierten Biologen und Alläuexperten Dr. Michael Schneider in Kooperation mit dem Deutschen Alpenverein (DAV), Sektion Augsburg.

## Montag 17.8.

### `19 Uhr` Auf dem Weg zur klimaschützenden und klimarobusten Stadt
Anforderungen an Bauen und Stadtplanung. *Vortrag von Christine Kamm, MdB.*


## Sonntag 16.8.

### `21 Uhr` Globale Klimaschulden der Industrieländer 
(Mathemathisch/Technisch anspruchsvoller Vortrag!)

Dr. Helmut Selinger (Ex-Mitarbeiter am Bundesprogramm der Klima- und Atmosphärenforschung & internationaler Klimaaktivist) hat die Klimaemmissionen für alle Länder der Welt!!! ausgerechnet und in Relation (nach Wirtschaftskraft, Bevölkerung, etc.) gestellt. Er will die Ergebnisse vorstellen und mit uns über ethische und realistische Verteilungsschlüssel diskutieren!

## Samstag 15.8.

### `15 Uhr` Workshop zum Unterschriftensammeln

## Freitag 14.8. -- **Tag 45**

* 7:15 Uhr: Wir wandeln eine Autofahrspur in einen Radweg!
* 13 Uhr: Solifoto für Aktionstag zu Datteln 4
* 15 Uhr: Live-Stream zum Protest in Datteln 4

## Donnerstag 13.8.

### `19 Uhr` Leben für Lau

## Dienstag 11.08.

### `19.00` „Brunos Märchenstunde“
Bruno Marcon hat in Augsburg mindestens 4 Bürgerbegeheren mitinitiiert, hat bei attac, der Wasserallianz und der Baumallianz Augsburgs Umwelt geschützt und mit der solidarischen Landwirtschaft ein Netzwerk gegründet, wo Stadt und Land zusammenwächst.

## Samstag 01.08. --  **1 Monat Klimacamp 🥳**

Am Samstag feiern wir unser einmonatiges Jubiläum mit einem großen Aktionstag!

## Freitag 31.07.2020
- 15:00 Mate statt Datteln — wir brühen Mate und sprechen über Datteln IV 🏭
- 17:00 Demozug durch die Innenstadt! (zu Fuß) 📣
- 18:00 Critical Mass (Fahrradkolonne durch die Stadt) 🚴‍♀️
- 19:00 Gemütlicher Abendausklang mit leckerem veganen Essen 🎶

## Donnerstag 30.07.2020
- 14:30 Weiterführung des Empathie-Workshops (auch Neulinge willkommen) 💕
- 15:00 Klimatanzen 💃
- 16:00 Besuch von Eva Lettenbauer (Landtagsmitglied für die Grünen) — Update zu Klimapolitik aus dem Landtag ℹ️
- 17:30 Workshop zu Ernährung und Klimagerechtigkeit 🥙
- 19:00 Gemütlicher Abendausklang mit leckerem veganen Essen 🎶

-------

Todo: Rücklick Tag 13 - 30 einfügen.

-------

## 26.7. -- **Tag 26**
**Rückblick:** 
- Vortrag zu „Der Verursacher und die Betroffene? Geschlechterperspektiven auf Klimagerechtigkeit“
- Diskussionsrunde und Vortrag mit Person von Rheinmetall Entwaffnen

## 24.7. -- **Tag 24**
**Rückblick:** Vortrag zu Kapitalismus & Klima

## 23.7. -- **Tag 23**
**Rückblick:**
Es gab am Klimacamp selbst einen Vortrag zu dem Thema „Klima, Fridays for Future und Corona. Chancen für den Wandel.“
- [Vortrag: Klima, Fridays for Future und Corona. Chancen für den Wandel.](https://www.youtube.com/watch?v=CMeMG0rwPOA)

## 17.7. -- **Tag 17**
**Rückblick:** Verwaltungsgericht entscheidet: Wir sind eine vom Versammlungsrecht geschützte Kundgebung (ach was). GRÜNE und CSU können uns jetzt nicht mehr räumen.

Medienberichte:
- [„Klima-Camp“ Augsburg: Aktivisten rechnen mit Eil-Entscheidung](https://www.br.de/nachrichten/bayern/klima-camp-augsburg-aktivisten-rechnen-mit-eil-entscheidung,S4yG3zh)
- [Verwaltungsgericht Augsburg: Klimacamp ist vom Versammlungsrecht gedeckt](https://www.daz-augsburg.de/verwaltungsgericht-klimacamp-ist-vom-versammlungsrecht-gedeckt/)

## 16.7. -- **Tag 16**
**Rückblick:** Vortrag zum Thema „100 % Erneuerbare Energieversorgung“ mit anschließender Podiumsdiskussion
Die Bewerbung lief etwas zu gut.
Aufgrund der COVID-19-bedingten Teilnehmerbeschränkungen
mussten Interessenten abgewiesen werden.
Einige warteten deutlich über eine Stunde,
um nach dem Vortrag und dem Weggang einiger Zuschauer
wenigstens an der Podiumsdiskussion noch teilnehmen zu können.
Der Vortrag liegt inzwischen als Video vor.
- [100 % Erneuerbare Energieversorgung in Deutschland -- (Wie) Geht das?](https://www.youtube.com/watch?v=AxjU6MzrbS8)

## 14.7. -- **Tag 14**
Medienberichte:
- [„Klimacamp“ neben dem Rathaus wird vorerst noch nicht geräumt](https://www.stadtzeitung.de/region/augsburg/politik/klimacamp-neben-rathaus-vorerst-noch-geraeumt-id209199.html)
- [Das Klimacamp am Augsburger Rathaus wird vorerst nicht geräumt](https://www.augsburger-allgemeine.de/augsburg/Das-Klimacamp-am-Augsburger-Rathaus-wird-vorerst-nicht-geraeumt-id57725176.html)

## Sonntag (12.7.) -- **Tag 12**
**Rückblick:**
* Oberbürgermeisterin Weber behauptet in einem [Facebook-Post](https://www.facebook.com/evaweberaugsburg/photos/a.1065950733447370/4072563629452717/?type=3&source=57) erneut, dass dass Klimacamp nicht mehr von der Versammlungsfreiheit gedeckt sei. Wir sind immer noch entsetzt.


## Samstag (11.7.) -- **Tag 11**
**Rückblick:**
* Das Camp wurde doch nicht geräumt
* Workshop Klimaplan von unten
* Workshop zu Soziokratie und Schüler*innen-Parlamenten
* lecker Kässpatzen und vegane Maultaschen
* Workshop grüne Städte
* Filmvorführung: [Radical Resilience](https://radicalresilience.noblogs.org/de/)

Medienberichte:
* [Politiker kritisieren angekündigte Räumung des Klimacamps](https://www.augsburger-allgemeine.de/augsburg/Politiker-kritisieren-angekuendigte-Raeumung-des-Klimacamps-id57713976.html)

## Freitag (10.7.) -- **Tag 10**
**Rückblick:** Merkwürdiges Demokratieverständnis bei CSU und GRÜNEN. Sie meinen, dass das Klimacamp nicht von der Versammlungsfreiheit gedeckt sei. Die beiden Bürgermeisterinnen Eva Weber (CSU) und Martina Wild (GRÜNE) kommen und bitten darum, das Camp aufzulösen. Machen wir nicht. Rainer Erben (GRÜNE), Norbert Stamm und Stefan X übergeben uns persönlich den Räumungsbescheid vom Hauptamt. Wir sind entsetzt über die Auffassung von GRÜNEN und CSU und reichen Klage beim Verwaltungsgericht ein. 

Medienberichte:
* [Die Stadt will das Klimacamp am Augsburger Rathaus räumen lassen](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-will-das-Klimacamp-am-Augsburger-Rathaus-raeumen-lassen-id57711651.html)
* [Augsburger Klimacamp: Die Stadt agiert kleinkariert](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Klimacamp-Die-Stadt-agiert-kleinkariert-id57712471.html)


## Donnerstag (9.7.) -- **Tag 9**
**Rückblick:**
* 18:00 Workshop der Bunten mit Daniel Tröster

## Mittwoch (8.7.) -- **Tag 8**
**Rückblick:**
* In einem informellen Vorabgespräch mit dem Ordnungsreferenten wurde angedeutet, dass kein Kundgebungscharakter erkennbar sei und daher Hausfriedensbuch vorliege. Das Morgenplenum war entsetzt über diese Behauptung.
* Das Ordnungsamt sagte den von ihm am Vortag ausgemachten Termin (12:30) ab.
* Möbelumstellung für mehr Ordnung, Programmtafel am Eingang
* Seminar zu Demokratie als Lebensform von Max und deborah
* Gutes Gespräch mit Stefan Sieber (Leiter Kommunikation der Stadt)
* Spontanbesuch von Claudia Roth (Grüne, Bundestagsvizepräsidenten), welche die Notwendigkeit des Camps unterstrich
* Fahrrad-Workshop von Christopher Papp/Radlkiste
* Band Wollstiefel am Abend
* Josef Hummel (CSU, Umweltausschussvorsitzender) gönnt sich das Essen der Soliküche
* Leckere Soliküche

## Dienstag (7.7.) -- **Tag 7**
**Rückblick:**
* Viele Plakate wurden gemalt, toller Eingangsbereich entstand
* Wir möchten vermehrt auch an anderen Orten als dem Rathausplatz Unterschriften sammeln (testweise Annastraße und City-Galerie; Plan zu Standaufbau bei erfolgreicherem Ort)
* Einladung von Pia Haertinger (Grüne), diversen Wissenschaftler*innen und anderen Gruppen (bis auf Pia passierte das noch nicht)
* Leider weniger Kontakt mit Fraktionsmitgliedern als erwartet
* Inspirierendes Brainstorming über zukünftige Aktionen
* Leckere Soliküche

Medienberichte:
- [Die Klimaschützer wollen ihr Camp am Rathaus noch lange betreiben](https://www.augsburger-allgemeine.de/augsburg/Die-Klimaschuetzer-wollen-ihr-Camp-am-Rathaus-noch-lange-betreiben-id57689296.html)

## Montag (6.7.) -- **Tag 6**
**Rückblick:**
* Workshop zu Rechtlichem zu Containern
* Leckere Soliküche
* Einladung von Oberbürgermeisterin Weber (CSU)

## Sonntag 5.7. --  **Tag 5**
**Rückblick:**
* Aktionstraining Ende Gelände am Nachmittag
* Critical Mass
* Workshop zu Rechtlichem zu Containern
* Leckere Soliküche
* Wohnzimmer
* Umbau für mehr Ordnung

## Samstag 4.7. -- **Tag 4**
**Rückblick:**
* Workshop zu Rechtlichem zu Containern
* Reifendemo (leider ohne Reifenholung, da eBay-Person zu spät reagierte)
* Leckere Soliküche

## Freitag 3.7. -- **Tag 3**
**Rückblick:**
* Regierung nimmt KohleEINstiegsgesetz im Bundestag an. Wir entscheiden zu bleiben.
* Bombendrohung gegen das Rathaus (nicht von uns)
* FFF-Demo um 11:00 Uhr vom Theater zum Camp
* Live-Musik von Itzy (Akkordion) und Wolle
* Leckere Soliküche

Medienberichte:
- [„Fridays for Future“: Klimaschützer melden sich mit Protesten zurück](https://www.augsburger-allgemeine.de/augsburg/Fridays-for-Future-Klimaschuetzer-melden-sich-mit-Protesten-zurueck-id57671656.html)

## Donnerstag 2.7. -- **Tag 2**
**Rückblick:**
* Aktionstraining Ende Gelände am Nachmittag
* Swarming von XR
* Rathausbesetzung
* Spontanter Demozug nach Rathausbesetzung
* Sponti und Besetzung zahlen sich aus: Auflage der Maximalteilnehmer_innenzahl (25 Leute) wurde gelockert
* Leckere Soliküche
* 20 Uhr weitere Demo

Medienberichte:
- [Klimaaktivisten besetzen Augsburger Rathaus](https://www.br.de/nachrichten/bayern/klimaaktivisten-besetzen-augsburger-rathaus,S3eOSKj)

## 01.07. -- **Tag 1 🚀**
**Regierung eskaliert komplett – wir reagieren.**
Nach bundesweiten Aktionen seitens FFF und anderen Klimabewegungen zeigt unsere Regierung immer noch keine Anzeichen dafür, dass das KohleEINstiegsgesetz nicht am Freitag durch den Bundestag geht. Wir stufen daher unser Aktionslevel hoch, um angemessen auf die Dringlichkeit reagieren zu können. Dieses Gesetz sorgt dafür, dass der deutsche Anteil zur Klimaerhitzung massiv über 1,5 °C hinausschießt. Wir fordern, dass sich die Fraktionen im Stadtrat öffentlichkeitswirksam und mit Nachdruck dagegen aussprechen.

Um mehr Druck auszuüben, platzieren wir uns daher ab Mittwoch, 01.07.2020, um 19 Uhr vor dem Rathaus und gehen erst wieder weg, wenn die Situation das zulässt. Damit wir die Zeit sinnvoll nutzen können, haben wir eine Kreideaktion mit extra hohem Spaßfaktor organisiert und werden einen spannenden Klimafilm anschauen. Außerdem haben wir viel Zeit um uns auszutauschen und neue Freund*innen zu finden. Jede*r kann dabei kommen und gehen, wann er*sie will. Einige Schüler*innen werden aber auf jeden Fall auch mindestens den Donnerstag streiken. Bis dann!

**Das ist passiert:** Campaufbau, Filmvorführung (Ende Gelände), Polizei erlässt Auflage von Maximalteilnehmer\*innenzahl von 25 Personen aber wir sind viel mehr.
