---
layout: page
title:  "7.5.2021: Klimacamper*innen widersprechen mit Banneraktion am CSU-Büro Abendlanduntergangsdarstellung von Oberbürgermeisterin Weber"
date:   2021-05-07 23:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 43
---

*Pressemitteilung vom Augsburger Klimacamp am 7. Mai 2021*

# Klimacamper*innen widersprechen mit Banneraktion am CSU-Büro Abendlanduntergangsdarstellung von Oberbürgermeisterin Weber

Am heutigen Freitagabend (7.5.2021) brachten
Klimagerechtigkeitsaktivist\*innen des Augsburger Klimacamps ein Banner
am Büro der Augsburger CSU an: "KOHLE HÄLT €SUSAMMEN -- NEIN ZUR KOHLE
-- NEIN ZUR LOBBY-UNION". Sie fordern damit ein Ende von Augsburgs
Beteiligung an der klimaschädlichen Kohleindustrie und spielen auf die
Korruptionsaffären der CSU an. "Mit der Banneraktion machen wir einen
konkreten Vorschlag für eine leicht umsetzbare Sofortmaßnahme für
Klimagerechtigkeit, die jährlich etwa ein Fünftel von Augsburgs
CO2-Emissionen einsparen könnte", so Mathematik-Dozent und Klimaaktivist
Dr. Ingo Blechschmidt (32, Universität Padova).

"Oberbürgermeisterin Weber setzt Ökostrom mit dem Untergang des
Abendlandes gleich", ordnet der Aktivist und angehende Förster Nico
Kleitsch (21) die Darstellungen von Oberbürgermeisterin Weber bei der
heutigen städtischen Pressekonferenz ein. In dieser zeichnete Weber das
Horrorszenario, dass mit einer Umstellung auf echten Ökostrom ein
massiver Arbeitsplatzverlust bei den Stadtwerken einherginge und
aufgrund geringerer Einnahmen im Energiesektor die ÖPNV-Ticketpreise
verdoppelt werden müssten.

"Wenn Frau Weber im Mathe-Unterricht besser aufgepasst hätte, hätte sie
sich heute nicht so verrechnet", kommentiert Blechschmidt diese
Einschätzung. Wie die Geschäftsleitung der Stadtwerke den
Klimacamper\*innen schon im Juli 2020 bei einem Gespräch im Camp
bestätigte, könnten die Stadtwerke ohne Strompreiserhöhung für ihre
Kund\*innen auf echten Ökostrom umsteigen, wenn sie nur jährlich 4 Mio. €
an zusätzlichen städtischen Fördermitteln bekämen. "Das sind vier
Promille des Jahresbudgets der Stadt", so Blechschmidt. Derzeit beziehen
die Stadtwerke etwa 40 Mio. € an jährlichen Fördermitteln. "Daraus sind
weder Arbeitsplatzverlust noch Verdopplung der ÖPNV-Ticketpreise
ableitbar."

Die heutige Banneraktion bildet den Auftakt einer Reihe von Aktionen
anlässlich der Klima-Sonderstadtratssitzung am kommenden DIenstag
(11.5.2021). Von den 34 Tagesordnungspunkten dienen 13 lediglich der
Berichterstattung, neue Maßnahmen werden mit ihnen nicht beschlossen.
"Der einzig interessante Punkt ist der Energiestandard. Drei weitere
Punkte waren lange überfällig, wie etwa die Umstellung der Stadtverwaltung
auf Recycling-Papier. Alle anderen Tagesordnungspunkte sind eine
Ohrfeige für alle, die sich für Klimagerechtigkeit engagieren, und das
Papier nicht wert, auf dem sie gedruckt werden", so Kleitsch. "Das
städtische CO2-Restbudget von 9,7 Mio. Tonnen lässt sich damit nicht
einhalten", bestätigt auch Klimabeiratsmitglied Blechschmidt.

## Fotos und Wideos zur freien Verwendung

https://www.speicherleck.de/iblech/stuff/.csu-aux-1/

## Hinweis

"Echter Ökostrom" ist von zertifikatsbasiertem Ökostrom zu
unterscheiden. Bei ersterem schließen Stadtwerke bilaterale Verträge mit
Ökostromproduzenten wie Betreibern von Windkraftanlagen, bei zweiterem
kaufen Stadtwerke lediglich gewisse Zertifikate ein. Dadurch steigt zwar
rein rechnerisch der eigene Ökostromanteil, dafür steigt andernorts aber
auch der Kohleanteil im Graustrom. Das lässt sich etwa nachlesen in
Anlage 3 (Abschnitt "Bezug von Ökostrom") der Beschlussvorlage
BSV/21/05703 der Sonderstadtratssitzung.

Die kürzlich angekündigte "Umstellung auf Ökostrom"
(https://www.sw-augsburg.de/magazin/detail/stadtwerke-augsburg-stellen-auf-oekostrom-um)
ist keine echte, sondern lediglich eine zertifikatsbasierte. 
